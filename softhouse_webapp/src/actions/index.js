import { SET_PROJECTS_DETAILS, API, FETCH_PROJECTS_DETAILS } from "./types";

export function fetchProjectsDetails() {
  return apiAction({
    url: "http://localhost:8080/allProjects",
    onSuccess: setProjectsDetails,
    onFailure: () => console.log("Error occured loading projects"),
    label: FETCH_PROJECTS_DETAILS
  });
}

function setProjectsDetails(data) {
  return {
    type: SET_PROJECTS_DETAILS,
    payload: data
  };
}

function apiAction({
  url = "",
  method = "GET",
  data = [],
  accessToken = null,
  onSuccess = () => {},
  onFailure = () => {},
  label = "",
  headersOverride = null
}) {
  return {
    type: API,
    payload: {
      url,
      method,
      data,
      accessToken,
      onSuccess,
      onFailure,
      label,
      headersOverride
    }
  };
}
