export const FETCH_PROJECTS_DETAILS = "FETCH_PROJECTS_DETAILS";
export const SET_PROJECTS_DETAILS = "SET_PROJECTS_DETAILS";

export const API = "API";
export const API_START = "API_START";
export const API_END = "API_END";
export const ACCESS_DENIED = "ACCESS_DENIED";
export const API_ERROR = "API_ERROR";
