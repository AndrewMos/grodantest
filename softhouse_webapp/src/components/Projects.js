import React from "react";
import { Tab, Row, Col, Nav, ListGroup } from "react-bootstrap";

const Description = (project) => {
  const startDate = new Date(project.startDate);
  const endDate = new Date(project.endDate);

  return (
    <Tab.Pane eventKey={project.id}>
      <h2 className="text-center bg-secondary text-white rounded p-4 m-1">
        {project.name}
      </h2>
      <h5 className="m-2">
        Start of project: {startDate.toLocaleDateString("en-US")}
      </h5>
      <h5 className="m-2">
        End of project: {endDate.toLocaleDateString("en-US")}
      </h5>
      <h3 class="text-center">Employees involved</h3>
      <ListGroup variant="flush">
        {project.employees.length ? (
          project.employees.map((employee, id) => (
            <ListGroup.Item>
              {id + 1}: {employee.name}
            </ListGroup.Item>
          ))
        ) : (
          <p className="text-center">No employees involved in the project</p>
        )}
      </ListGroup>
    </Tab.Pane>
  );
};

const Project = (project) => {
  return (
    <Nav.Item>
      <Nav.Link eventKey={project.id}>{project.name}</Nav.Link>
    </Nav.Item>
  );
};

const Projects = ({ projects = [] }) => {
  return (
    <Tab.Container defaultActiveKey={projects.length && projects[0].id}>
      <Row>
        <Col sm={3}>
          <Nav variant="pills" className="flex-column">
            {Array.from(projects).map((project) => Project(project))}
          </Nav>
        </Col>
        <Col sm={9}>
          <Tab.Content>
            {Array.from(projects).map((project) => Description(project))}
          </Tab.Content>
        </Col>
      </Row>
    </Tab.Container>
  );
};

export default Projects;
