import React, { Component } from "react";
import { connect } from "react-redux";
import Projects from "../components/Projects";
import { fetchProjectsDetails } from "../actions";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar, Container } from 'react-bootstrap';

class App extends Component {
  state = {};
  componentDidMount() {
    this.props.fetchProjectsDetails();
  }
  render() {
    return (
      <div>
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand>Project Team Viewer</Navbar.Brand>
        </Navbar>
        <Container className="mt-4">
          {this.props.isLoadingData ? (
            "Loading . . ."
          ) : (
            <Projects
              projects={this.props.data}
            />
          )}
        </Container>
      </div>
    );
  }
}

const mapStateToProps = ({ data = {}, isLoadingData = false }) => ({
  data,
  isLoadingData
});
export default connect(
  mapStateToProps,
  {
    fetchProjectsDetails
  }
)(App);
