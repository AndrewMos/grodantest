import {
  SET_PROJECTS_DETAILS,
  API_START,
  API_END,
  FETCH_PROJECTS_DETAILS
} from "../actions/types";

// eslint-disable-next-line import/no-anonymous-default-export
export default function(state = {}, action) {
  console.log("action type => ", action.type);
  switch (action.type) {
    case SET_PROJECTS_DETAILS:
      return { data: action.payload };
    case API_START:
      if (action.payload === FETCH_PROJECTS_DETAILS) {
        return {
          ...state,
          isLoadingData: true
        };
      }
      break;
    case API_END:
      if (action.payload === FETCH_PROJECTS_DETAILS) {
        return {
          ...state,
          isLoadingData: false
        };
      }
      break;
    default:
      return state;
  }
}
