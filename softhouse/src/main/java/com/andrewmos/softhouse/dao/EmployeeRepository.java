package com.andrewmos.softhouse.dao;

import com.andrewmos.softhouse.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "*", maxAge = 3600)
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}


