package com.andrewmos.softhouse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SofthouseApplication {

	public static void main(String[] args) {
		SpringApplication.run(SofthouseApplication.class, args);
	}

}
