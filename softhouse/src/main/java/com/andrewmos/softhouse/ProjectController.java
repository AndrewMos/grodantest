package com.andrewmos.softhouse;

import com.andrewmos.softhouse.dao.ProjectRepository;
import com.andrewmos.softhouse.entity.Project;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class ProjectController {
    private final ProjectRepository repository;

    ProjectController(ProjectRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/allProjects")
    List<Project> all() {
        return repository.findAll();
    }
}
