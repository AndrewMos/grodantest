package com.andrewmos.softhouse.entity;

// generates getter and setters automatically
import lombok.Data;
import org.springframework.web.bind.annotation.CrossOrigin;
//

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name="project")
@Data
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @ManyToMany
    @JoinTable(
            name = "employees",
            joinColumns = @JoinColumn(name = "project_id"),
            inverseJoinColumns = @JoinColumn(name = "employee_id")
    )
    private Set<Employee> employees;

    @Column(name="name")
    private String name;

    @Column(name="start_date")
    private Date startDate;

    @Column(name="end_date")
    private Date endDate;


}
