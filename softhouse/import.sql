INSERT INTO `softhouse`.`employee` (name)
		Values('John Doe');
INSERT INTO `softhouse`.`employee` (name)
		Values('Snow Whit');
INSERT INTO `softhouse`.`employee` (name)
		Values('Jack Jackie');
INSERT INTO `softhouse`.`employee` (name)
		Values('Samiha Duggan');
INSERT INTO `softhouse`.`employee` (name)
		Values('Anisha Hodgson');
INSERT INTO `softhouse`.`employee` (name)
		Values('Samah Clements');
INSERT INTO `softhouse`.`employee` (name)
		Values('Niyah Pennington');
INSERT INTO `softhouse`.`employee` (name)
		Values('Catriona Quintana');
INSERT INTO `softhouse`.`employee` (name)
		Values('Lance Mcknight');
INSERT INTO `softhouse`.`employee` (name)
		Values('Allana Mcfadden');
INSERT INTO `softhouse`.`employee` (name)
		Values('Derek Coates');
INSERT INTO `softhouse`.`employee` (name)
		Values('Leona Rowley');
INSERT INTO `softhouse`.`project` (name, start_date, end_date)
		Values('Ecommerce book storage project', '2018-05-25', '2019-03-02');
INSERT INTO `softhouse`.`project` (name, start_date, end_date)
		Values('Project for counting cats', '2019-11-12', '2020-09-13');
INSERT INTO `softhouse`.`project` (name, start_date, end_date)
		Values('Secret project', '2015-12-31', Now());
INSERT INTO `softhouse`.`project` (name, start_date, end_date)
		Values('Project for blind people', '2018-05-25', '2019-03-02');
INSERT INTO `softhouse`.`project` (name, start_date, end_date)
		Values('Non-commertial project', '2019-11-12', '2020-09-13');
INSERT INTO `softhouse`.`project` (name, start_date, end_date)
		Values('Hobby project', '2015-12-31', Now());
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(1, 1);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(1, 2);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(1, 3);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(2, 1);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(2, 2);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(4, 1);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(4, 2);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(4, 3);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(4, 4);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(4, 5);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(4, 6);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(4, 7);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(4, 8);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(4, 9);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(4, 10);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(4, 11);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(4, 12);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(5, 5);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(5, 6);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(5, 7);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(5, 8);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(5, 9);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(5, 10);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(6, 1);
INSERT INTO `softhouse`.`employees` (project_id, employee_id)
		Values(6, 5);